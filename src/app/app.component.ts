import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  
  title = 'wordleAngular';
  wordToGuessTab: string[]=["chien","chats","levre","rafut","codes","lapin","lourd"];
  wordToGuess: string = "";
  tabWordToGuess: string[] = Array.from(this.wordToGuess);
  wordTried = "";
  wordGuessedSoFar = "*****";
  tabWordGuessedSoFar = Array.from(this.wordGuessedSoFar);
  tabAnswers: string[][]=[];
  nbTryLeft = 7;
  messageDefeatOrVictory:string='';
  victory:boolean=true;
  disable=false;
  visible=false;

  ngOnInit(): void {
    if(this.nbTryLeft==7){
      this.findWordToGuess();
      this.tabWordToGuess= Array.from(this.wordToGuess);
    }
   
  }
  findWordToGuess(){
    let index= Math.floor(Math.random()*6);
    this.wordToGuess=this.wordToGuessTab[index];
   }
  
  decrementTry() {
    this.nbTryLeft--;  
  }
  printWord() {
    console.log(this.wordTried);
  }
  emptyField() {
    this.wordTried = "";
  }
  checkIfLettersAreInRightPlace() {
    let tabWordTried: string[] = Array.from(this.wordTried);
    for (let i = 0; i < this.tabWordToGuess.length; i++) {
      if(this.tabWordToGuess[i]==tabWordTried[i]){
        this.tabWordGuessedSoFar[i]=this.tabWordToGuess[i];
        this.tabWordGuessedSoFar[i]=this.tabWordGuessedSoFar[i].toUpperCase();
        this.tabWordToGuess[i]='/';
      }
    }
    console.log("word to guess: "+this.tabWordToGuess);
    console.log("word guessed so far: "+this.tabWordGuessedSoFar);
  
  }
  SearchForLettersInWrongPlace(){
    let tabWordTried: string[] = Array.from(this.wordTried);
    for (let i = 0; i < this.tabWordToGuess.length; i++) {
      for (let j = 0; j < tabWordTried.length; j++) {
        if(this.tabWordToGuess[i]==tabWordTried[j]){
          this.tabWordGuessedSoFar[j]=tabWordTried[j];
          this.tabWordToGuess[i]='#';
        } 
      }    
    }
    console.log(" 2 word to guess: "+this.tabWordToGuess);
    console.log("2 word guessed so far: "+this.tabWordGuessedSoFar);
    this.wordGuessedSoFar=this.tabWordGuessedSoFar.join('');
    this.tabAnswers.push(this.tabWordGuessedSoFar);
    this.refreshWordGuessedSoFar(); 
    this.refreshTabWordToGuess();

  }
  checkVictoryCondition(){
    if(this.nbTryLeft==0){
      this.messageDefeatOrVictory="Vous avez perdu, désolé";
      this.victory=false;
      this.endTheGame();
    }
  }
  checkIfPlayerWon(){
    let tabWordTried: string[] = Array.from(this.wordTried);
    let nbLetterfound=0;
    for (let i = 0; i < this.tabWordToGuess.length; i++) {
      if(this.tabWordToGuess[i].toUpperCase()==tabWordTried[i].toUpperCase()){
        nbLetterfound++;
      }
      
    }
    if(nbLetterfound==5){
      this.messageDefeatOrVictory="Vous avez gagné, félicitation!!";
      this.victory=true;
      this.endTheGame();
      
    }
  }
  refreshTabWordToGuess(){
    this.tabWordToGuess= Array.from(this.wordToGuess);
  }

  refreshWordGuessedSoFar(){
    this.wordGuessedSoFar= "*****";
    this.tabWordGuessedSoFar = Array.from(this.wordGuessedSoFar);
  }
 
  endTheGame(){
    //disable input and button ...
    this.disable=true;
    this.visible=true;  
  }
  startNewGame(){
    location.reload();
  }


}

