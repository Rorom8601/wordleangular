# WordleAngular

J'ai converti mon jeu Wordle codé en Java en Angular. Je me suis contenté d'un seul composant (app.component.ts).

# Partie Front


J'ai utilisé bootstrap uniquement pour créer 2 colonnes, mon site n'est pas responsive, il aurait fallu que je redimentionne mon input principal ainsi que la police des caractères qui la compose.

Sur la colonne de gauche, il y a de brèves explications des règles suivies de l'input principal dans lequel le joueur entrera son mot proposé. Suivent ensuite le bouton de validation et le nombre d'essais restant.

La colonne de droite est réservée aux essais successifs, comme indiqué dans les règles, les lettres bien placées apparaisent en vert et les mal placées en jaune.

# Partie Angular

J'ai utilisé le double binding avec nbModel pour lier mon input au mot proposé. Il est lié à la propriétè "wordTried" déclaré partie js. D'une manière générale, j'ai transformé les mots en tableau de string pour pouvoir les comparer.
J'ai déclaré un tableau de string dans lequel je stocke les mots à deviner. Une fonction prévue à cet effet, tire au hasard un des mots de cette liste. Ce sera le mot à deviner.

Sur le click du bouton "Valider", j'appelle successivement une série de fonction, la première, "checkIfLettersAreInRightPlace()" permet de détecter les lettres à la bonne place, puis "SearchForLettersInWrongPlace()" qui a pour rôle de detecter les lettres mal placées. Je ne m'attarde pas sur l'algorythme car c'est le même que dans le projet Jeux. Sont ensuite appelées les fonctions pour décrémenter les nombre d'essais (initialisé à 7 en début de partie). Si ce nombre tombe à 0, la partie est perdu suite à l'appel de la fonction "checkVictoryCondition()". Sur chaque click, les fonction "checkIfPlayerWon()" et "emptyField()" sont aussi appelées. Si le mot entré coïncide avec le mot à deviner, le joueur a gagné.

A la fin d'une partie, un message est affiché différemment selon l'issue de la partie (victoire où défaite). Ce message correspond à la propriété this.messageDefeatOrVictory. J'ai utilisé ngClass pour afficher le message de défaite en rouge et celui de victoire en vert. Je me suis servi de la même technique pour faire apparaître le bouton "Rejouer" (qui passe de caché à visible) qui recharge la page. A la fin d'une partie, je rend "disable" le champ de saisie ainsi que le bouton validation grâce à la proprièté de type booleènne "disable" en .ts.

## Affichage du résultat de la comparaison

Dans la colonne de droite, est indiqué le résultat de la comparaison entre le mot entré et le mot à trouver dans la div #myElement. Cet élément est composé d'un *ngFor dans lequel est stocké l'ensemble des mots essayés pour que le joueur puisse voir l'ensemble des essais. A l'intérieur de chaque mot (qui est en fait un tableau de string, typeScript ne traitant pas le Char), j'ai inséré des spans (élement inline), avec un *ngFor je "stylise" la lettre en fonction de sa présence à la bonne place dans le mot à deviner (vert) ou simplement sa présence à un mauvais emplacement (jaune). 

# Améliorations possibles

Je suis conscient que le fait d'appeler une série de fonctions à partir du bouton n'est certainement la la meilleure solution, j'aurais pu les appelées en cascade coté ts. Le site n'est pas responsive, pour ce faire, j'aurais du redimensionner l'input de saisie, la taille des caractères le contenant, ainsi que la taille des caractères de la liste des mots tentés. Le fait de recharger la page n'est pas optimal, cela peut engendrer des lenteurs et empêche d'avoir un système de scoring, mais dans les délais impartis, et étant donné ma faible maîtrise d'Angular, j'ai priorisé cette solution. 

